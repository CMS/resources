Exportiert von 

 - warumlinux.de (bzw.)
 - https://pad.uugrn.org/p/WarumLinux


# Warum Linux? 

https://warumlinux.de

Sammlung von Gründen in einfacher Sprache, warum Linux für andere möglicherweise das beste Betriebssystem sein könnte.
Bitte frei ergänzen:

## Umwelt und Klimaschutz (Nachhaltigkeit)

- Alte Computer können mit Linux noch viel länger benutzt werden als mit anderen Betriebssystemen.
- Es gibt Linux-Varianten, die extrem wenig Strom benötigen (Hier wäre ein Link auf einen Stromvergleich hilfreich).
- Keine unerwünscht vorinstallierte Software, die sich meist auch nicht ohne weiteres entfernen lässt  und die unnötig Speicher und Leistung benötigt. (Solche Software wird auch Bloatware genannt.)
- Im Gegensatz zu anderen gängigen Betriebssystemen werden Daten nicht automatisch an die Entwickler des Betriebssystems gesendet. Daten, die nicht fließen, verbrauchen auch keinen Strom.  Das spart Energie.
- Viele Drucker oder Scanner funktionieren mit Linux. Insbesondere ältere Geräte.

## Sicherheit/Weiterentwicklung/Zukunftssicherheit 

- Alte Computer und Peripheriegeräte können sicher weiterverwendet werden.
- Jeder kann den Quellcode einsehen.
- Linux wird weltweit von einer großen Anzahl an Entwickler*innen programmiert und weiterentwickelt.
- Sicherheitslücken werden so schneller gefunden und die Fehler korrigiert.
- Es gibt sehr lange Aktualisierungen.
- Es gibt täglich Sicherheitsupdates und das Einspielen geht schnell.
- Ein Neustart des Systems nach dem Update ist nur selten nötig.
- Wenn ein Neustart nach einem Update nötig ist, kann der Zeitpunkt selbst entschieden werden.
- Linux ist quelloffene Software (Open Source). Das bedeutet, dass jeder den Quellcode einsehen kann.
- Linux ist vom Grundesign her schon auf Sicherheit ausgelegt.
- Linux ist immer verfügbar, für alle. Man muss sich nicht irgendwo anmelden dafür.
- Ich habe die Freiheit die Software zu nutzen, wie ich es möchte. Der Hersteller kann sie nicht einfach einstellen und die Downloaddateien aus dem Internet nehmen, so dass ich eine neue Version kaufen muss.
- Die Gefahr sich zweifelhafte oder Schadsoftware zu installieren ist geringer.

## Datenschutz und Privatsphäre

- Kein Online-Aktivierungszwang.
- Nutzung ohne Online-Konto möglich.
- Kein erzwungenes Nach-Hause-Telefonieren.
- Keine zentrale Sammlung von Nutzerdaten und deren Nutzungsverhalten.
- Keine Anzeige von Werbung.
- Keine automatische ungefragte Installation von Apps und Programmen.
- Keine unerwünscht vorinstallierte Software, die sich meist auch nicht ohne weiteres entfernen lässt. (Bloatware)
- Die Daten liegen auf dem eigenen Computer oder auf dem gewünschten Server im Internet (Cloud). Es gibt keinen Zwang, Dateien in der Cloud zu speichern.
- Linux ist quelloffene Software (Open Source).

## Sozialer/finanzieller Aspekt

- Jede*r kann es verwenden.
- Die meisten Linux-Varianten (Distributionen) sind kostenlos nutzbar. 
- Kauf-Varianten (Distributionen) zahlt man einmal (meist ohne Abo-Modell, bei dem regelmäßig gezahlt werden muss.)
- Viele gute Programme und Anwendungen auf Profiniveau gibt es kostenlos.
- Es gibt Computer zu kaufen, mit vorinstalliertem Linux. Meistens sind diese Geräte auch besser reparierbar. Das schont auch die Umwelt.
- Es gibt Vereine und Organisationen, die alte Computer annehmen und Linux darauf installieren. Die werden dann an Menschen ausgegeben, die sich keinen Computer leisten können. Siehe https://technikspenden.de
- Viele Linux-Varianten (Distributionen) und Linux-Programme wurden in viele unterschiedliche Sprachen übersetzt. Teilweise auch in seltene Sprachen.
- Linux-Varianten (Distributionen) und Linux-Programme lassen sich in der Regel leicht in neue Sprachen übersetzen.
- Für Linux gibt es in fast allen erdenklichen Anwendungsbereichen Software.
- Die meisten Linux-Varianten enthalten einen so genannten Paket-Manager. Damit kann man sehr viele Programme installieren, die genau für diese Linux-Variante angepasst und getestet wurden. Das Suchen nach Software entfällt weitgehend.
- Ein Paket-Manager vermeidet außerdem das versehentliche Mit-Installieren von unerwünschten Programmen und ermöglicht es, ohne unseriöse Download-Seiten auszukommen.

## Wissen/Lernen/Einstieg

- Linux ist leicht erlernbar für Einsteigerinnen und Einsteiger ohne Computererfahrung.
- Es gibt viele Ressourcen für den Lernprozess.
- Der Umstieg auf Linux ist möglich. Wer voher Windows oder macOS benutzt hat, findet sich schnell zurecht.
- Es gibt weltweit eine große Unterstützergemeinde und Unterstützung in Foren, Chats, etc.
- In vielen Städten gibt es Gruppen, die bei Fragen oder Problemen helfen können. Sie veranstalten auch Einsteigerabende, wo jeder vorbei kommen kann. Suche nach Linux- oder Unix-Usergroup.
- Linux lässt sich ausprobieren. Viele Linux-Varianten lassen sich vom USB-Stick oder DVD starten und ausprobieren, ohne das vorhandene Betriebssystem zu beeinflussen.
- Viele freie Anwendungen, die es für Linux gibt, stehen auch schon für andere Betreibssysteme zur Verfügung. So kann der Umstieg Schrittweise erfolgen. Beispiel: LibreOffice, Thunderbird, Firefox, Gimp.
- Linux erleichtert einen Einstieg in das tiefere Verständnis von Computern. Zum Beispiel wenn man mehr lernen möchte und vorher Endnutzer von Software und Betriebssystem war.
- Die Installation geht ähnlich einfach oder gar einfacher als bei anderen Betriebssystemen.
- Wenn du Linux früher schon einmal ausprobiert hast und nicht begeistert warst, dann lohnt es sich den aktuellen Stand anzusehen. In den letzten Jahren hat sich viel getan und viele Distributionen sind heutzutage nutzerfreundlicher gestaltet. Du kannst, musst aber nicht mit der Kommandozeile und der Eingabe von Befehlen arbeiten.
- Bei Problemen mit dem Betriebssystem sind im Internet Hilfestellungen zu finden, die mögliche Ursachen erklären und Lösungsmöglichkeiten anbieten.

## Allgemein

- Linux lässt sich so einstellen, dass es ähnlich wie andere Betriebssysteme aussieht oder sich so verhält.
- Ich kann es grafisch so auf meine Vorlieben und die Arbeitsweise anpassen. Also das Aussehen so gestalten, wie ich es möchte.
- Freie Wahl der Benutzeroberfläche aus einer Vielzahl von Möglichkeiten (z.B. KDE https://kde.org/de/, GNOME https://www.gnome.org/, XFCE https://www.xfce.org/, ...).
- Die meisten Linux-Varianten (Distributionen) sind unabhängig von einer Firma.
- Sehr gute Multimedia-Fähigkeiten: das Audio-Subsystem ist besser als das anderer Betriebssysteme
- Offene Standards für alle möglichen Dateiformate. Kein Zwang dauerhaft eine bestimmte Software wegen der Dateiformate zu verwenden (Lock-in-Syndrom).
- Es lassen sich einfach Daten mit dem Handy, Smartphone oder Tablet austauschen. Sogar, wenn es von Apple ist.
- Das Nutzen von Linux sensibilisiert für Themen wie Datenschutz, da das Betriebssystem Einsichten in alle seine Ebenen erlaubt. Dadurch kann eingesehen werden, was alles (Gutes & Schlechtes) mit Daten angefangen werden kann
- Eigene Serversysteme lassen sich relativ einfach zu Hause hosten. (Headless) Beispiele wären hier das NAS-System Openmediavault oder den Werbeblocker PiHole

## Unklar/unpräzise/noch einmal überarbeiten

- Es funktioniert einfach.
- Freie Kommunikation und Interaktion für freie, mündige Menschen.

## Begriffsklärung

- Bloatware / "Keine Bloatware" bezieht sich auf Software oder Geräte, die nur die notwendigen Funktionen bieten und keine unnötigen Ressourcen beanspruchen.
- kein Lock-in-Syndrom:  ermöglicht flexibel zwischen verschiedenen Softwareanwendungen und Plattformen wechseln zu können, ohne an eine bestimmte Technologie gebunden zu sein.

## Literatur

- kurz&mündig: Einfach. Linux. Dieses Heft zeigt, wie einfach Linux zu installieren ist und welche Programme zur Auswahl stehen: https://digitalcourage.de/kum/band17_linux

## Ressourcen

- https://www.linuxwiki.de/LinuxKnowledgeBase
- https://linuxkurs.ch/




