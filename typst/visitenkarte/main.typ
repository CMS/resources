#import "@preview/codetastic:0.2.2": qrcode;
#let page_size = (width: 8.5cm, height: 5.3cm);
#set text(font: "PT Sans");
#set page(
  ..page_size,
  background: [
    #image(
      // src: https://www.freepik.com/free-ai-image/cute-fish-ice-cream-fantasy-backdrop-generated-by-ai_41213126.htm#query=seamless%20chaos&position=34&from_view=keyword&track=ais
      "background.jpg",
      width: 255pt,
      height: 180pt)
    ],
    // to make the back side 
    margin: (left: -1pt, right: 0pt)
  );

#let front_content = [#image("cms.png", width: 120pt, height: auto)]];

#let round_dots(diff: length) = {
  return (thickness: 2pt, cap: "round", dash: (0pt, diff));
};

#let transparent = color.linear-rgb(0,0,0,0)

#let cell = rect.with(
  radius: 6pt,
  stroke: none
);

#let shadow_box(body, fill: luma(150), shadow-color: luma(240), shadow: 3pt, shadow-radius: 2pt, width: 170pt, height: 50pt) = {
  rect(fill: fill, stroke: (paint: transparent), width: width + shadow, height: height + shadow, inset: (left: -2pt, top: -2pt), radius: 1.5 * shadow-radius)[
    #align(top + left)[
      #rect(fill: shadow-color, stroke: (paint: black, thickness: 0.5pt), inset: (left: 5pt, right:0pt), width: width, height: height, radius: shadow-radius)[
        #body
      ]
    ]
  ]
};

#let info_content = [
  #set text(size: 8pt);
  #grid(
    columns: (20%, 70%),
    row-gutter: -6pt,
    rows: auto,
      cell[*Email:*], cell[schule\@c3d2.de],
      cell[*Adresse:*], cell[Zentralwerk, Riesaer Straße 32],
      cell[*Treffen:*], cell[zweiter Mittwoch im Monat],
      cell[], cell[letzter Donnerstag im Monat],
  );
]

#let other_content = [
  #text(size: 8pt)[Du hast Lust bei uns mit-zuwirken oder möchtest allgemein gern mehr zu Chaos macht Schule wissen? Scanne den Code und schau auf unserer Seite vorbei!]
];

#let circle_radius = 68pt;

#align(
  center + horizon)[
    #circle(
      fill: white,
      stroke: (paint: black, thickness: 0.75pt),
      radius: circle_radius)[
        #circle(
          fill: white,
          // magic number to make the circle seemless
          stroke: (paint: luma(100), ..round_dots(diff: 3.0pt)),
          radius: circle_radius - 4pt)[
            #front_content  
        ]
    ]
]

#align(left + horizon)[
  #rect(fill: white, width: 193pt, height: 200pt, inset: 0pt, stroke: black)[
    #rect(fill: white, stroke: (paint:black, thickness: 0.6pt), width: 190pt, height: 175pt, inset: 0pt)[
      #box(inset: (left: 12pt, top: 4pt, right: 0pt))[
        #align(center)[
          #shadow_box(shadow-radius: 2pt, shadow: 1pt)[
            #align(left + horizon)[
              #info_content
            ]
          ]
        ]
        #rect(fill: white, stroke: white, inset: (left: 0pt, top: -13pt))[
          #box(width: 100pt)[
            #other_content
          ]
          #box(width: 60pt, inset: (left: 5pt, right: -10pt, bottom: -5pt, top: 5pt))[
            #text(size: 5.5pt)[https://c3d2.de/schule.html]
            #qrcode("https://c3d2.de/schule.html", width: 60pt)
          ]
        ]
      ] 
    ]    
  ]
]
