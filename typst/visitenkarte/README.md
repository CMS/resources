# How to render it yourself

- typst.app
- get an account 
- start empty project 
- copy everything from main.typ to the file 
- upload the two images there as well 
- done 

# Is there a way to do this not in the web app?

- Certainly! I haven't tried it yet. Feel free to find it out!

# Useful

- Docs @ https://typst.app/docs/reference/
