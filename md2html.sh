#!/bin/bash

# Wir erstellen mal lieber ne Kopie mit der wir arbeiten
mach_ma_lieber_eine_kopie() {
    local filename="$1"
    local timestamp=$(date +"%Y%m%d%H%M%S")
    local backup_markdown_file="${filename}.${timestamp}.bak"
    cp "$filename" "$backup_markdown_file"
    echo "$backup_markdown_file"
}

# Frage nach dem Dateinamen der Markdown-Datei und speichere ihn in markdown_file
if [ $# -eq 1 ]; then
	markdown_file=$1
else
	read -p "nac gib den Dateinamen der Markdown-Datei ein: " markdown_file
fi

# Überprüfe, ob die Markdown-Datei existiert
if [ ! -f "$markdown_file" ]; then 
    echo "Die Markdown-Datei '$markdown_file' existiert nicht."
    exit 1
fi 

# Kopie der Original Markdown Datei erstellen und damit weiterarbeiten
backup_markdown_file=$(mach_ma_lieber_eine_kopie "$markdown_file")

# Erstelle das Verzeichnis, falls es noch nicht existiert
mkdir -p img
# Durchsuche die Markdown-Datei nach Bild-URLs und speichere sie in urls
urls=$(grep -oE 'https?://[^)]+\.(png|jpg|jpeg|gif|webp)' "$backup_markdown_file")
# Durchlaufe alle gefundenen URLs
for url in $urls; do
    # Extrahiere den Dateinamen aus der URL und speichere ihn in filename
    filename=$(basename "$url")
    # Lade die Datei herunter und speichere sie im 'img'-Verzeichnis
    curl -o "img/$filename" -L "$url"
    # Ersetze die URL im Markdown-File durch den lokalen Dateipfad
    local_filepath="img/$filename"
    sed -i "s|$url|$local_filepath|g" "$backup_markdown_file"
done
echo "Alle Mediendateien wurden heruntergeladen und die Pfadangaben in der Markdown-Datei wurden aktualisiert."

echo "Erstelle Offline HTML Slides"
reveal-md "$backup_markdown_file" --static "${markdown_file%.*}-slides" --static-dirs=img/
echo "Done... html/"

# Kopiere Bilder die reveal-md nicht sauber übernommen hat (img tag im md Dokument
#cp -r "img/" "html/"

# Benenne mal besser die von reveal-md mit übernommene md Datei wieder um
mv "${markdown_file%.*}-slides/$backup_markdown_file" "${markdown_file%.*}-slides/$markdown_file"
