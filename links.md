# Wilde Linksammlung

- Das ist eine wilde Linksammlung mit allerlei Ressourcen fuer Vortraege, Projekte und Empfehlungen 
- Wenn jemand ein bestimmtes Thema ein bisschen ausarbeiten will, dann legt gern ein eigenes Markdown file dafuer an und verlinkt es hier, wenn moeglich
- Waere cool, wenn ab und an mal jemand ueber die Links schaut, um zu checken ob sie noch funktionieren

## [Digital Courage - Free Software Snack Buffet](https://digitalcourage.de/kinder-und-jugendliche/schulen/freie-software-snack-buffet)

- Reihe von Weiterbildungen fuer Lehrkraefte zum Thema FOSS (Free Open Source Software)
- Enthaelt links zu anderen Resourcen
- [Materialien von bereits stattgefundenen Veranstaltungen sind auch zu finden](https://cloud.digitalcourage.de/s/ExN3BSc6Dffdw3D)

## [OhMyGit](https://ohmygit.org/)

- FOSS Spiel um visuell git zu lernen
- Zielgruppe Kids
- Wird gerade (2023) ueberarbeitet sodass v2 in Aussicht steht

## [Elara](https://github.com/albrow/elara)

- FOSS Spiel fuer Kinder, um Programmieren zu lernen
- Hab ich noch nicht naeher ausgecheckt, sieht interssant aus
